#include <iostream>

template <typename T>
T AddNumbers(T left, T right) {
  return left + right;
}

int main() {
  std::cout << AddNumbers(3, 5) << std::endl;
  std::cout << AddNumbers(3.1, 5.0) << std::endl;
  std::cout << AddNumbers(0.1f, 1.01f) << std::endl;
}
